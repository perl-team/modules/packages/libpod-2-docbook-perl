libpod-2-docbook-perl (0.03-4) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/rules: use 'wildcard' instead of 'shell echo'.
  * Rename autopkgtest configuration file(s) as per new pkg-perl-
    autopkgtest schema.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.

  [ gregor herrmann ]
  * Add libpod-parser-perl to {build,runtime} dependencies.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.
  * Use HTTPS for bugtracker URL in debian/upstream/metadata.
  * Add lintian overrides (false positive about version number which is
    part of the name).

 -- gregor herrmann <gregoa@debian.org>  Sun, 17 May 2020 13:12:17 +0200

libpod-2-docbook-perl (0.03-3) unstable; urgency=low

  * Team upload

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Damyan Ivanov ]
  * Mark package as autopkgtest-able
  * Declare compliance with Debian Policy 3.9.6
  * Add explicit build dependency on libmodule-build-perl
  * skip t/distribution.t in autopkgtest
  * fix comma-separated files in d/copyright

 -- Damyan Ivanov <dmn@debian.org>  Mon, 08 Jun 2015 20:22:49 +0000

libpod-2-docbook-perl (0.03-2) unstable; urgency=low

  * Don't run t/valid_xml.t, needs network access (closes: #628359).
  * Set Standards-Version to 3.9.2 (no changes).
  * Bump debhelper compatibility level to 8.
  * Update spelling of DocBook in doc-base abstract.

 -- gregor herrmann <gregoa@debian.org>  Sat, 28 May 2011 21:34:41 +0200

libpod-2-docbook-perl (0.03-1) unstable; urgency=low

  * Initial release (closes: #608236).

 -- gregor herrmann <gregoa@debian.org>  Wed, 29 Dec 2010 02:38:36 +0100
